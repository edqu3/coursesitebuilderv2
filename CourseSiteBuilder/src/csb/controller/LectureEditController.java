package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.LectureItem;
import csb.gui.CSB_GUI;
import csb.gui.LectureItemDialog;
import csb.gui.MessageDialog;
import csb.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Eduardo Quispe
 */
public class LectureEditController {
    
    LectureItemDialog lid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;

    public LectureEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }
    
    public void handleAddLectureItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showAddLectureItemDialog();
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // GET THE LECTURE ITEM
            LectureItem li = lid.getLectureItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addLecture(li);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditLectureItemRequest(CSB_GUI gui, LectureItem itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showEditLectureItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            LectureItem li = lid.getLectureItem();
            itemToEdit.setTopic(li.getTopic());
            itemToEdit.setSessions(li.getSessions());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveLectureItemRequest(CSB_GUI gui, LectureItem itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }    
    

    
    
}
