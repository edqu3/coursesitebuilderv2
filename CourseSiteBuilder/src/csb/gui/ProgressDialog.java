package csb.gui;

import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Eduardo Quispe
 */
public class ProgressDialog extends Stage{

    static final String CLASS_PROGRESS_BAR = "progress_bar";
    static final String CLASS_PROGRESS_PIE = "progress_pie";
    
    GridPane          gridPane;
    Label             messageLabel;
    ProgressBar       progressBar;
    ProgressIndicator progressIndicator;
    Scene             dialogScene;
    
    public ProgressDialog(Stage primaryStage) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        setResizable(false);
        
        setWidth(440);
        setHeight(240);
        
        gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        messageLabel = new Label("Exporting...");
        messageLabel.setFont(Font.font(null, 24));
        
        progressBar = new ProgressBar(0.0);
        progressBar.getStyleClass().add(CLASS_PROGRESS_BAR);
        
        progressIndicator = new ProgressIndicator(0.0);
        progressIndicator.getStyleClass().add(CLASS_PROGRESS_PIE);

        HBox hBox = new HBox();
        hBox.getChildren().add(progressBar);
        hBox.getChildren().add(progressIndicator);
        hBox.setAlignment(Pos.CENTER);
        
        gridPane.add(messageLabel, 0,0,2,1);
        gridPane.add(hBox,         0,1,2,1);
        
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    // destroys this dialog
    public void destroy(){
        this.close();
    }
    
    public void update(String msg, double perc){
        messageLabel.setText("Exporting " + msg + " Completed");
        progressBar.setProgress(perc);
        progressIndicator.setProgress(perc);
    }
    
}