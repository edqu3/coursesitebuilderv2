/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.data.Course;
import csb.data.LectureItem;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Eduardo Quispe
 */
public class LectureItemDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    LectureItem lectureItem;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Label headingLabel;
    Label topicLabel;
    Label sessionsLabel;
    Scene dialogScene;
    ComboBox sessionsComboBox;
    TextField topicTextField;
    Button completeButton;
    Button cancelButton;    
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    private static final int   MAX_SESSIONS            = 3;
    public static final String COMPLETE                = "Complete";
    public static final String CANCEL                  = "Cancel";
    public static final String TOPIC_PROMPT            = "Topic: ";
    public static final String SESSIONS_PROMPT         = "Number of Sessions: ";
    public static final String LECTURE_ITEM_HEADING    = "Lecture Details";
    public static final String ADD_LECTURE_ITEM_TITLE  = "Add New Lecture Item";
    public static final String EDIT_LECTURE_ITEM_TITLE = "Edit Lecture Item";    
    
    /**
     * Input dialog box for adding/editing/removing LectureItem objects.
     * @param primaryStage
     * @param course
     * @param messageDialog 
     */
    public LectureItemDialog(Stage primaryStage, Course course, MessageDialog messageDialog) {
        // dialog box maintains forces focus until closed.
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // contents of grid pane here
        headingLabel = new Label(LECTURE_ITEM_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);        
        // Topic
        topicLabel = new Label(TOPIC_PROMPT);
        topicLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicTextField = new TextField();
        topicTextField.textProperty().addListener( (observable, oldValue, newValue) -> {
            lectureItem.setTopic(newValue);
        });
        // Sessions
        sessionsLabel = new Label(SESSIONS_PROMPT);
        sessionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        sessionsComboBox = new ComboBox();
        sessionsComboBox.valueProperty().addListener( (observable, oldValue, newValue) -> {
            lectureItem.setSessions((int)newValue);
        });
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton   = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            LectureItemDialog.this.selection = sourceButton.getText();
            LectureItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(topicLabel, 0, 1, 1, 1);
        gridPane.add(topicTextField, 1, 1, 1, 1);
        gridPane.add(sessionsLabel, 0, 2, 1, 1);
        gridPane.add(sessionsComboBox, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);
        
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
        
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public LectureItem getLectureItem() { 
        return lectureItem;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @return 
     */
    public LectureItem showAddLectureItemDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_LECTURE_ITEM_TITLE);
        
        // RESET THE LECTURE ITEM OBJECT WITH DEFAULT VALUES
        lectureItem = new LectureItem();
        
        // LOAD THE UI STUFF
        topicTextField.setText(lectureItem.getTopic());
        
        // is ObservableList necessary? or does a simple array suffice?
        ObservableList<Integer> sessions = FXCollections.observableArrayList();
        for (int i = 1; i <= MAX_SESSIONS; i++) { sessions.add(i); }
        sessionsComboBox.setItems(sessions);
        sessionsComboBox.setValue(lectureItem.getSessions());
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return lectureItem;
    }
    
    public void loadGUIData(){
        //LOAD THE UI STUFF
        topicTextField.setText(lectureItem.getTopic());
        
        // is ObservableList necessary? or does a simple array suffice?
        ObservableList<Integer> sessions = FXCollections.observableArrayList();
        for (int i = 1; i <= MAX_SESSIONS; i++) { sessions.add(i); }
        sessionsComboBox.setItems(sessions);
        sessionsComboBox.setValue(lectureItem.getSessions());
    }
    
    public boolean wasCompleteSelected(){
        // reset selection back to CANCEL, otherwise X'ing out of the 
        // LectureItemDialog triggers COMPLETE for the next call and the default
        // LectureItem is added to the LectureItemTable
        if (selection.equals(COMPLETE)) {
            selection = CANCEL;
            return true;
        }else{
            return false;            
        }    }
    
    public void showEditLectureItemDialog(LectureItem itemToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_LECTURE_ITEM_TITLE);
        
        // LOAD THE LECTURE ITEM INTO OUR LOCAL OBJECT
        lectureItem = new LectureItem();
        lectureItem.setTopic(itemToEdit.getTopic());
        lectureItem.setSessions(itemToEdit.getSessions());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
        
        // AND OPEN IT UP
        this.showAndWait();
    }
    
}
